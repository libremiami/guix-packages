# LibreMiami's Guix Channel

## Setup

Add the snippet below to your `~/.config/guix/channels.scm`:

```scheme
  (cons* (channel
          (name 'libremiami-packages)
          (url "https://gitlab.com/libremiami/guix-channel.git"))
         %default-channels)
```

Then run `guix pull`.

## Contributing

Contributions are welcome! If there's a package you would like to add,
just fork the repository and create a Merge Request when your package
is ready. Keep in mind:

* libremiami-packages follows the same
  [coding style](https://www.gnu.org/software/guix/manual/en/html_node/Coding-Style.html)
  as GNU Guix.  If you don't use Emacs, you should make use of the indent
  script from the GNU Guix repository (`./etc/indent-code.el`).

* Commit messages should follow the same
  [conventions](https://www.gnu.org/prep/standards/html_node/Change-Logs.html)
  set by GNU Guix.

### Applying For Channel Authorization

* Open an issue sharing your OpenPGP key so that I can make you an [authorized developer](https://guix.gnu.org/manual/en/html_node/Specifying-Channel-Authorizations.html).

* Use [guix git authenticate](https://guix.gnu.org/manual/en/html_node/Invoking-guix-git-authenticate.html) to authenticate your commits.

See [here](https://guix.gnu.org/manual/en/html_node/Creating-a-Channel.html) to read more about the importance of channel authorization.

## Current Packages

- [terminal_shit](https://giit.cyberia.club/~vvesley/terminal_shit) - A terminal client for [shitchat](https://giit.cyberia.club/~j3s/shitchat).

- [vis-guava](https://gitlab.com/libremiami/vis-guava) - A Miami-inspired color scheme for vis.

- [luke-st](https://github.com/LukeSmithxyz/st) - Luke Smith's fork of the suckless terminal with vim bindings and Xresource compatibility.

- [emacs-ed-mode](https://github.com/ryanprior/ed-mode) - ed for Emacs.

- [python-beautysh](https://github.com/lovesegfault/beautysh) - A Bash beautifier for the masses.

## Community

If you want to discuss topics related to libremiami-packages, come hang out and stay in touch on `#libremiami:matrix.org`.