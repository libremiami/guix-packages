(package
  (name "python-searx")
  (version "0.16.0")
  (source
    (origin
      (method url-fetch)
      (uri (pypi-uri "searx" version))
      (sha256
        (base32
          "1ck0ksrjkjf9gsa6p7d38c6n2x9sl78wkgm0qzligzk2myfghwr1"))))
  (build-system python-build-system)
  (propagated-inputs
    `(("python-babel" ,python-babel)
      ("python-certifi" ,python-certifi)
      ("python-dateutil" ,python-dateutil)
      ("python-flask" ,python-flask)
      ("python-flask-babel" ,python-flask-babel)
      ("python-idna" ,python-idna)
      ("python-jinja2" ,python-jinja2)
      ("python-lxml" ,python-lxml)
      ("python-pygments" ,python-pygments)
      ("python-pyopenssl" ,python-pyopenssl)
      ("python-pyyaml" ,python-pyyaml)
      ("python-requests" ,python-requests)))
  (native-inputs
    `(("python-cov-core" ,python-cov-core)
      ("python-linuxdoc" ,python-linuxdoc)
      ("python-mock" ,python-mock)
      ("python-nose2" ,python-nose2)
      ("python-pallets-sphinx-themes"
       ,python-pallets-sphinx-themes)
      ("python-pep8" ,python-pep8)
      ("python-plone.testing" ,python-plone.testing)
      ("python-selenium" ,python-selenium)
      ("python-sphinx" ,python-sphinx)
      ("python-sphinx-issues" ,python-sphinx-issues)
      ("python-sphinx-jinja" ,python-sphinx-jinja)
      ("python-sphinx-tabs" ,python-sphinx-tabs)
      ("python-splinter" ,python-splinter)
      ("python-transifex-client"
       ,python-transifex-client)
      ("python-unittest2" ,python-unittest2)
      ("python-zope.testrunner"
       ,python-zope.testrunner)))
  (home-page "https://github.com/asciimoo/searx")
  (synopsis
    "A privacy-respecting, hackable metasearch engine")
  (description
    "A privacy-respecting, hackable metasearch engine")
  (license #f))

