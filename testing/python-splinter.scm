(define-public python-splinter
  (package
    (name "python-splinter")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "splinter" version))
        (sha256
          (base32
            "0mm35i6f625l8xyhkanjrjsygkp9n0xjqqapq5x508in4xgm74ly"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-selenium" ,python-selenium)
        ("python-six" ,python-six)))
    (native-inputs
      `(("python-cssselect" ,python-cssselect)
        ("python-lxml" ,python-lxml)
        ("python-zope.testbrowser"
         ,python-zope.testbrowser)))
    (home-page
      "https://github.com/cobrateam/splinter")
    (synopsis
      "browser abstraction for web acceptance testing")
    (description
      "browser abstraction for web acceptance testing")
    (license #f)))
