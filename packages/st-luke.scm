(define-module (st-luke)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages xorg)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix packages))

(define-public st-luke
  (let ((revision "1")
  (commit "8ab3d03681479263a11b05f7f1b53157f61e8c3b"))
  (package
    (name "st-luke")
    (version (git-version "0.0.0" revision commit))
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
            (url "https://github.com/LukeSmithxyz/st")
            (commit commit)))
            (file-name (git-file-name name version))
       (sha256
        (base32 "1brwnyi1hr56840cdx0qw2y19hpr0haw4la9n0rqdn0r2chl8vag"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f                      ; no tests
       #:make-flags
       (list (string-append "CC=" ,(cc-for-target))
             (string-append "PREFIX=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-after 'unpack 'inhibit-terminfo-install
           (lambda _
             (substitute* "Makefile"
               (("\ttic .*") ""))
             #t)))))
    (inputs
     `(("libx11" ,libx11)
       ("libxft" ,libxft)
       ("fontconfig" ,fontconfig)
       ("harfbuzz" ,harfbuzz)
       ("freetype" ,freetype)))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (home-page "https://st.suckless.org/")
    (synopsis "Luke's build of st - the simple (suckless) terminal")
    (description
     "Luke's fork of the suckless simple terminal (st) with vim bindings and Xresource compatibility. ")
    (license license:x11))))

st-luke
