(define-module (vis-guava)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system copy))

(define-public vis-guava
(let ((commit "b4b5d46c5a8571869a1f3a529d3c45afe828e66b")
        (revision "1"))
    (package
      (name "vis-guava")
      (version "0.1")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://gitlab.com/libremiami/vis-guava")
               (commit commit)))
         (file-name (string-append name "-" commit))
         (sha256
          (base32
           "1hf37fk1hklg741mipnnb7nyr1jlfs7lqsbqwg8vh2fqwfd7v3ja"))))
      (build-system copy-build-system)
      (arguments
       '(#:install-plan
         '(("guava.lua" "share/vis/themes/"))))
      (synopsis "A LibreMiami theme for the vis text editor")
      (description
       "@code{vis-guava} is a Miami-inspired theme for vis.")
      (home-page "https://gitlab.com/libremiami/vis-guava")
      (license license:gpl3+))))

vis-guava
