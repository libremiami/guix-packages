(define-module (terminal-shit)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system go)
  #:use-module ((guix licenses) #:prefix license:))

(define-public terminal-shit
  (let ((commit "36cfd6e756a7ce73e84810ca37b9ffd43a69fd78"))
    (package
      (name "terminal_shit")
      (version commit)
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://giit.cyberia.club/~vvesley/terminal_shit")
                      (commit commit)))
                (sha256
                 (base32
                  "0b8v4fnwdk0djd58cdz4bvvam64mlrs84yda8lcmv68dbsjlzh1a"))
                (file-name (git-file-name name version))))
      (build-system go-build-system)
      (arguments
        '(#:tests? #f
          #:import-path "giit.cyberia.club/~vvesley/terminal_shit"))
      (home-page "https://giit.cyberia.club/~vvesley/terminal_shit")
      (synopsis "shitchat client in the terminal")
      (description "@code{terminal_shit} is a chat client that can be run in two modes: in and out.")
      (license license:gpl3))))

terminal-shit
