(define-module (emacs-ed-mode)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix build-system emacs)
  #:use-module (gnu packages)
  #:use-module (gnu packages emacs-xyz)
  #:use-module (gnu packages emacs))

(define-public emacs-ed-mode
  (let ((commit "69f4fb34eca8df6a3bfe24bd8d8075551f0264ac"))
    (package
      (name "emacs-ed-mode")
      (version "1.0")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/ryanprior/ed-mode")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "058siccx6znw1bcp820bll0jg300xz8w1wf97zr1fa6jwfxyhhvi"))))
      (build-system emacs-build-system)
;      (native-inputs
;       `(("ert-runner" ,emacs-ert-runner)))
;      (arguments
;       `(#:tests? #t
;         #:test-command '("emacs" "-Q" "-batch" "-l" "ed-mode-tests.el" "-f" "ert-run-tests-batch-and-exit")))
      (home-page "https://github.com/ryanprior/ed-mode")
      (synopsis "ed for Emacs")
      (description "ed-mode lets you interact with Emacs buffers like you would do with the ed editor.")
      (license license:gpl3+))))

emacs-ed-mode
