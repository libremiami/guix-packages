(define-module (python-beautysh)
#:use-module (guix packages)
#:use-module (guix utils)
#:use-module (gnu packages python)
#:use-module (gnu packages python-xyz)
#:use-module (guix download)
#:use-module ((guix licenses) #:prefix license:)
#:use-module (guix build-system python))

(define-public python-beautysh
  (package
    (name "python-beautysh")
    (version "6.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "beautysh" version))
        (sha256
          (base32
            "1qa5q560cfi5wkca261ksqrm2mv99jzw0sjmfv5hyizjhijzvwlx"))))
    (build-system python-build-system)
    (arguments
      `(#:phases
         (modify-phases %standard-phases
          (delete 'check))))
    (home-page
      "https://github.com/bemeurer/beautysh")
    (synopsis "A Bash beautifier for the masses.")
    (description "A Bash beautifier for the masses.")
    (license license:expat)))

python-beautysh

